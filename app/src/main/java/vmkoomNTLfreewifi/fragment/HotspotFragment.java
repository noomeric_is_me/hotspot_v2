package vmkoomNTLfreewifi.fragment;

import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.Settings;

import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.crashlytics.android.Crashlytics;

//import com.facebook.ads.Ad;
//import com.facebook.ads.AdChoicesView;
//import com.facebook.ads.AdError;
//import com.facebook.ads.AdListener;
import com.free.ap.freehotspot.personal.mobilehotspotfree.BuildConfig;
import com.google.android.ads.nativetemplates.NativeTemplateStyle;
import com.google.android.ads.nativetemplates.TemplateView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
//import com.facebook.ads.InterstitialAd;
//import com.facebook.ads.InterstitialAdListener;
//import com.facebook.ads.MediaView;
//import com.facebook.ads.NativeAd;
import com.free.ap.freehotspot.personal.mobilehotspotfree.R;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
//import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.mady.wifi.api.wifiHotSpots;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ListHolder;
import com.orhanobut.dialogplus.OnBackPressListener;
import com.orhanobut.dialogplus.OnCancelListener;
import com.orhanobut.dialogplus.OnClickListener;
import com.orhanobut.dialogplus.OnDismissListener;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.orhanobut.dialogplus.ViewHolder;
import com.thefinestartist.Base;

//import fast.cleaner.battery.saver.MainActivity;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import cn.pedant.SweetAlert.SweetAlertDialog;
import utils.AppUtils;
import vmkoomNTLfreewifi.Splashscreen;
import vmkoomNTLfreewifi.activity.SpeedTestMiniActivity;
import vmkoomNTLfreewifi.application.AppController;
import vmkoomNTLfreewifi.main.IntroActivity;
import vmkoomNTLfreewifi.main.MainAppActivity_v2;
import vmkoomNTLmainapp.scrollable.fragment.FragmentPagerFragment;
import wijaofihotspot8.MagicActivity;

import static android.app.Activity.RESULT_OK;
import static com.thefinestartist.utils.content.ContextUtil.getApplicationContext;
import static com.thefinestartist.utils.content.ContextUtil.getPackageName;
import static com.thefinestartist.utils.service.ServiceUtil.getWindowManager;

/**
 * Created by intag pc on 2/12/2017.
 */

public class HotspotFragment extends FragmentPagerFragment {
    private Button btinstruction_oreo, bthotspot_off, bthotspot_off2, cancelbutton;
    public static final String TAG = HotspotFragment.class.getSimpleName();
    public static final String AP_NAME_WIFI_HOTSPOT_TYPE_KEY = "ap_name_wifi_hotspot_type_key";
    public static final String AP_PASSWORD_WIFI_HOTSPOT_TYPE_KEY = "ap_password_wifi_hotspot_type_key";

    // Remote Config keys
    private static final String CONFIG_KEY_NATIVE_LARGE_DISPLAY = "display_native_large";
    private static final String CONFIG_KEY_NATIVE_SMALL_DISPLAY = "display_native_small";
    private FirebaseRemoteConfig mFirebaseRemoteConfig;

    private NestedScrollView mScrollView;

    public static HotspotFragment newInstance() {
        return new HotspotFragment();
    }

    private wifiHotSpots mHotUtil;
    public static boolean mWifiHotspotEnable;
    private SharedPreferences sharedPref;
    private String vmkoomNameWifiHotspot, vmkoomPasswordWifiHotspot;
    private ImageView vmkoomImageToggle;
    private EditText vmkoomPasswordInput, vmkoomSSIDInput;
    private CheckBox vmkoomShowPasswordCheckbox;
    private TextView vmkoomSettingSave, vmkoomSettingInformation;
    private Switch vmkoomSecuritySwitchToggle;
    ImageView mainbrush, cache, temp, residue, system;
    TextView maintext, cachetext, temptext, residuetext, systemtext, hotspoton_alert;
    public static ImageView mainbutton;
    private Handler mHandler;
    private CardView hotspotconfig, oreo_instruction;
    private int i = -1;
    int checkvar = 0;
    int alljunk;
    //Ad
    private AdView adView;
//    private InterstitialAd interstitialAd;
//    private NativeAd nativeAd;
    private LinearLayout nativeAdContainer;
    private LinearLayout adViewLinearLayout;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    private FrameLayout adContainerView;
    private AdView adtView;
    View view;

    @Nullable
    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                mRootView= inflater.inflate(R.layout.hotspot_oreo_main, container, false);
//            } else {
//                mRootView= inflater.inflate(R.layout.hotspot_main, container, false);
//            }
//            mRootView= inflater.inflate(R.layout.hotspot_main, container, false);
            view = inflater.inflate(R.layout.hotspot_main, container, false);
            mScrollView = view.findViewById(R.id.scrollView);
            mHandler = new Handler();
            mHotUtil = new wifiHotSpots(getActivity());

            // Create Remote Config Setting to enable developer mode.
            mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();

            // Create Remote Config Setting to enable developer mode.
            // Fetching configs from the server is normally limited to 5 requests per hour.
            // Enabling developer mode allows many more requests to be made per hour, so developers
            // can test different config values during development.
            FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings
                    .Builder()
                    .setDeveloperModeEnabled(BuildConfig.DEBUG)
                    .build();
            mFirebaseRemoteConfig.setConfigSettingsAsync(configSettings);

            // Set default Remote Config values. In general you should have in app defaults for all
            // values that you may configure using Remote Config later on. The idea is that you
            // use the in app defaults and when you need to adjust those defaults, you set an updated
            // value in the App Manager console. Then the next time you application fetches from the
            // server, the updated value will be used. You can set defaults via an xml file like done
            // here or you can set defaults inline by using one of the other setDefaults methods.
            mFirebaseRemoteConfig.setDefaultsAsync(R.xml.remote_config_defaults);
//            initremoteconfig();
//        ImageView iispeedtestii = (ImageView) view.findViewById(R.id.speedtestii);
//        iispeedtestii.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View mRootView) {
//                final Intent intent;
//                intent = new Intent(getActivity(), IntroActivity.class);
//                startActivity(intent);
//            }
//        });
            TemplateView template = view.findViewById(R.id.my_template);
            if (mFirebaseRemoteConfig.getBoolean(CONFIG_KEY_NATIVE_LARGE_DISPLAY)) {
                //show native
                template.setVisibility(View.VISIBLE);
//                Toast.makeText(getActivity(), "CONFIG_KEY_NATIVE_LARGE_DISPLAY - True", Toast.LENGTH_SHORT).show();
            } else {
                //show native
                template.setVisibility(View.GONE);
//            Toast.makeText(getActivity(), "CONFIG_KEY_NATIVE_LARGE_DISPLAY - False", Toast.LENGTH_SHORT).show();
            }
            oreo_instruction = this.view.findViewById(R.id.oreo_instruction);
            hotspotconfig = this.view.findViewById(R.id.confighotspot);
            btinstruction_oreo = (Button) view.findViewById(R.id.BTinstruction_oreo);
            hotspoton_alert = (TextView) view.findViewById(R.id.hotspoton_alert);
            hotspoton_alert.setVisibility(View.GONE);
            btinstruction_oreo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View mRootView) {
                    oreoinstruction_dialog();
                }
            });
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                hotspotconfig.setVisibility(View.GONE);
//                oreo_instruction.setVisibility(View.VISIBLE);
                btinstruction_oreo.setVisibility(View.VISIBLE);
            } else {
                hotspotconfig.setVisibility(View.VISIBLE);
//                oreo_instruction.setVisibility(View.GONE);
                btinstruction_oreo.setVisibility(View.GONE);
            }

//
//            bthotspot_off = (Button) view.findViewById(R.id.HotspotOFF);
//            bthotspot_off.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View mRootView) {
//                    vmkoomStartWifiHotspot();
////                    MagicActivity.useMagicActivityToTurnOn(Objects.requireNonNull(getActivity()));
//                }
//            });
//            bthotspot_off2 = (Button) view.findViewById(R.id.HotspotOFF2);
//            bthotspot_off2.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View mRootView) {
//                    vmkoomStopWiFiHotspot();
////                    MagicActivity.useMagicActivityToTurnOff(Objects.requireNonNull(getActivity()));
//                }
//            });
            vmkoomImageToggle = view.findViewById(R.id.vmkoom_image_toggle);
            vmkoomImageToggle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View mRootView) {
                    vmkoomShareWifiHotspot();
                }
//                @Override
//                public void onClick(View mRootView) {
//                    try {
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
//                                .setTitleText("Are you sure?")
//                                .setContentText("Won't be able to recover this file!")
//                                .setCancelText("No,cancel plx!")
//                                .setConfirmText("Yes,delete it!")
//                                .setNeutralText("Setting")
//                                .showCancelButton(true)
//                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                                    @Override
//                                    public void onClick(SweetAlertDialog sDialog) {
//                                        vmkoomShareWifiHotspot();
//                                        sDialog.cancel();
//                                    }
//                                })
//                                .setNeutralClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                                    @Override
//                                    public void onClick(SweetAlertDialog sDialog) {
//                                        final Intent intent = new Intent(Intent.ACTION_MAIN, null);
//                                        intent.addCategory(Intent.CATEGORY_LAUNCHER);
//                                        final ComponentName cn = new ComponentName("com.android.settings", "com.android.settings.TetherSettings");
//                                        intent.setComponent(cn);
//                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                        startActivity(intent);
//                                    }
//                                })
//                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                                    @Override
//                                    public void onClick(SweetAlertDialog sDialog) {
////                                        vmkoomShareWifiHotspot();
//                                        sDialog.cancel();
//                                    }
//                                })
//                                .show();
////                        new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
////                                .setTitleText("Not support on this Device!")
////                                .setContentText("We will update hotspot function to support Android 8, 9 as soon")
////                                .setConfirmText("Dismiss")
////                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
////                                    @Override
////                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
////                                        sweetAlertDialog.dismissWithAnimation();
////                                    }
////                                })
////                                .show();
//                    }else{
//                        vmkoomShareWifiHotspot();}
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M&&Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
//
//                        if (!Settings.System.canWrite(getActivity().getApplicationContext())) {
//                            AlertDialog.Builder alertadd = new AlertDialog.Builder(getActivity());
//                            LayoutInflater factory = LayoutInflater.from(getActivity());
//                            final View view = factory.inflate(R.layout.diaglog_settingpermission, null);
//                            alertadd.setView(view);
//
//                            alertadd.setNeutralButton("OK!", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dlg, int sumthin) {
//                                    settingPermission();
//                                }
//                            });
//
//                            alertadd.show();
//                        }
//                    }
//
//                    } catch (Exception ignored) {
//                        Crashlytics.logException(ignored);
//                    }
//                }
            });
//            Base.initialize(getActivity());
            vmkoomConfigureHotspot();
//            MobileAds.initialize(getActivity(), getResources().getString(R.string.admob_app_id));
//            refreshAd();
            initAds();

        }
        return view;

    }

    /**
     * Called when leaving the activity
     */
    @Override
    public void onPause() {
        if (adtView != null) {
            adtView.pause();
        }
        super.onPause();
    }

    /**
     * Called when returning to the activity
     */
    @Override
    public void onResume() {
        super.onResume();
        if (adtView != null) {
            adtView.resume();
        }
    }

    @Override
    public void onDestroy() {
//        if (adViewfacebook != null) {
//            adViewfacebook.destroy();
//        }
        if (adtView != null) {
            adtView.destroy();
        }
        super.onDestroy();
    }

    private void settingPermissiondialog() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!Settings.System.canWrite(getActivity().getApplicationContext())) {
                    AlertDialog.Builder alertadd = new AlertDialog.Builder(getActivity());
                    LayoutInflater factory = LayoutInflater.from(getActivity());
                    final View view = factory.inflate(R.layout.diaglog_settingpermission, null);
                    alertadd.setView(view);

                    alertadd.setNeutralButton("OK!", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dlg, int sumthin) {

                        }
                    });

                    alertadd.show();
                }
            }
        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }
    }

    public void settingPermission() {
        try {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!Settings.System.canWrite(getApplicationContext())) {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:" + getPackageName()));
                    startActivityForResult(intent, 200);

                }
            }
        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }
    }

//    private com.facebook.ads.AdView adViewfacebook;

    //    private NativeAdsManager manager;
//    private NativeAdScrollView nativeAdScrollView;
    private void initAds() {

        if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
            //Banner
//            AdView mAdView = (AdView) view.findViewById(R.id.admob_banner_view);
//            mAdView.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showAdsBanner(mAdView);

            adContainerView = view.findViewById(R.id.ad_view_container);
            adContainerView.setVisibility(View.VISIBLE);
            // Since we're loading the banner based on the adContainerView size, we need to wait until this
            // view is laid out before we can get the width.
            adContainerView.post(new Runnable() {
                @Override
                public void run() {
                    loadBanner();
                }
            });

//            showMob();
            //load large ads native bottom
            refreshAd();

//            mPublisherAdView = view.findViewById(R.id.admob_banner_view);
//            PublisherAdRequest adRequest = new PublisherAdRequest.Builder().build();
//            mPublisherAdView.setVisibility(View.VISIBLE);
//            mPublisherAdView.loadAd(adRequest);

//            mPublisherAdView = (PublisherAdView) view.findViewById(R.id.fluid_view);
//            PublisherAdRequest publisherAdRequest = new PublisherAdRequest.Builder().build();
//            mPublisherAdView.loadAd(publisherAdRequest);
            //load large ads native bottom
//            NativeExpressAdView adViewNative = (NativeExpressAdView) mView.findViewById(R.id.adViewNative);
//            adViewNative.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showNativeAdsBanner(adViewNative);

//            NativeExpressAdView adViewNative_1 = (NativeExpressAdView) mView.findViewById(R.id.adViewNative_1);
//            adViewNative_1.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showNativeAdsBanner(adViewNative_1);
        } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
            //Banner
            RelativeLayout adViewContainer = (RelativeLayout) view.findViewById(R.id.facebook_banner_ad_container);
            adViewContainer.setVisibility(View.VISIBLE);
//            adViewfacebook = AppUtils.getInstance().showFBAdsBanner(AppController.getInstance().getAppContext(), adViewContainer);

            //Native
            LinearLayout adNativeViewContainer = (LinearLayout) view.findViewById(R.id.hotspot_native_ad_container);
            adNativeViewContainer.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showFBAdsNativeSmallInit(AppController.getInstance().getAppContext(), adNativeViewContainer);

        }
    }

    public void showInterstitial() {
        try {
            if (AppUtils.ads_interstitial_show_all) {

                if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
                    AppUtils.getInstance().showAdsFullBanner(null);
                }

            } else {

                if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {

                    AppUtils.getInstance().showAdmobAdsFullBanner(null);

                } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {

//                    AppUtils.getInstance().showFBAdsFullBanner(null);

                }
            }
        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }
    }

    private void vmkoomConfigureHotspot() {
        sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        vmkoomNameWifiHotspot = sharedPref.getString(AP_NAME_WIFI_HOTSPOT_TYPE_KEY, "FreeWiFi");
        vmkoomPasswordWifiHotspot = sharedPref.getString(AP_PASSWORD_WIFI_HOTSPOT_TYPE_KEY, "11111111");

        vmkoomSettingInformation = this.view.findViewById(R.id.vmkoom_setting_information);
        vmkoomSettingInformation.setVisibility(View.GONE);

        vmkoomSSIDInput = this.view.findViewById(R.id.vmkoom_edit_SSID);
        vmkoomSSIDInput.setText(vmkoomNameWifiHotspot);

        vmkoomPasswordInput = this.view.findViewById(R.id.vmkoom_edit_password);
        vmkoomPasswordInput.setText(vmkoomPasswordWifiHotspot);

        vmkoomSecuritySwitchToggle = this.view.findViewById(R.id.vmkoom_security_switch_toggle);
        vmkoomSecuritySwitchToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                if (checked) {
                    vmkoomPasswordInput.setEnabled(true);
                    vmkoomPasswordInput.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
                    vmkoomShowPasswordCheckbox.setEnabled(true);
                    vmkoomShowPasswordCheckbox.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
                } else {
                    vmkoomPasswordInput.setEnabled(false);
                    vmkoomPasswordInput.setTextColor(ContextCompat.getColor(getActivity(), R.color.gray));
                    vmkoomShowPasswordCheckbox.setEnabled(false);
                    vmkoomShowPasswordCheckbox.setTextColor(ContextCompat.getColor(getActivity(), R.color.gray));
                }
            }
        });

        vmkoomSettingSave = this.view.findViewById(R.id.vmkoom_setting_save);
        vmkoomSettingSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View mRootView) {

//                String tempNameWifiHotspot = sharedPref.getString(AP_NAME_WIFI_HOTSPOT_TYPE_KEY, "FreeWiFi");
//                String tempPasswordWifiHotspot = sharedPref.getString(AP_PASSWORD_WIFI_HOTSPOT_TYPE_KEY, "11111111");

                vmkoomNameWifiHotspot = vmkoomSSIDInput.getText().toString();
                vmkoomPasswordWifiHotspot = vmkoomPasswordInput.getText().toString();

//                if (tempNameWifiHotspot.equals(vmkoomNameWifiHotspot) && tempPasswordWifiHotspot.equals(vmkoomPasswordWifiHotspot)) {
//                    return;
//                }

                Toast.makeText(getActivity(), "Save!", Toast.LENGTH_LONG).show();

                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(AP_NAME_WIFI_HOTSPOT_TYPE_KEY, vmkoomNameWifiHotspot);
                editor.apply();
                editor.putString(AP_PASSWORD_WIFI_HOTSPOT_TYPE_KEY, vmkoomPasswordWifiHotspot);
                editor.apply();

                if (mWifiHotspotEnable) {
                    mHotUtil.setAndStartHotSpotCheckAndroidVersion_v2(false, vmkoomNameWifiHotspot, vmkoomPasswordWifiHotspot);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            vmkoomStartWifiHotspot();
                        }
                    }, 2000);
                }
            }
        });

        vmkoomPasswordInput.addTextChangedListener(
                new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.toString().trim().length() > 7 && vmkoomSSIDInput.length() > 0) {
                            vmkoomSettingSave.setEnabled(true);
                            vmkoomSettingSave.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorTextCanSave));
                        } else {
                            vmkoomSettingSave.setEnabled(false);
                            vmkoomSettingSave.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorTextCannotSave));
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                });

        vmkoomSSIDInput.addTextChangedListener(
                new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.toString().trim().length() > 0 && vmkoomPasswordInput.length() > 7) {
                            vmkoomSettingSave.setEnabled(true);
                            vmkoomSettingSave.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorTextCanSave));
                        } else {
                            vmkoomSettingSave.setEnabled(false);
                            vmkoomSettingSave.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorTextCannotSave));
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                });

        // Toggling the show password CheckBox will mask or unmask the password input EditText
        vmkoomShowPasswordCheckbox = this.view.findViewById(R.id.vmkoom_showPassword);
        vmkoomShowPasswordCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (!vmkoomShowPasswordCheckbox.isChecked()) {
                    vmkoomPasswordInput.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    vmkoomPasswordInput.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    vmkoomPasswordInput.setInputType(InputType.TYPE_CLASS_TEXT);
                    vmkoomPasswordInput.setTransformationMethod(null);
                }
            }
        });
    }

    private static int requestCode_ACTION_MANAGE_WRITE_SETTINGS = 200;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == requestCode_ACTION_MANAGE_WRITE_SETTINGS) {
            if (resultCode == RESULT_OK) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!Settings.System.canWrite(getApplicationContext())) {
                        vmkoomRetryOpenWifiHotspot();
                    } else {
                        vmkoomStartWifiHotspot();
                    }
                }
            }
        }
    }

    private void vmkoomRetryOpenWifiHotspot() {
        final CoordinatorLayout coordinatorLayout;
        coordinatorLayout = view.findViewById(R.id.scrollView);
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, "Unable to Open Free Hotspot", Snackbar.LENGTH_LONG)
                .setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View mRootView) {
                        vmkoomStartWifiHotspot();
                    }
                });

        snackbar.show();
    }

    private void vmkoomShareWifiHotspot() {

        try {

            if (vmkoomSSIDInput.length() < 1) {
                Toast.makeText(getActivity(), "No network name",
                        Toast.LENGTH_LONG).show();
                return;
            }

            if (vmkoomSecuritySwitchToggle.isChecked()) {
                if (vmkoomPasswordInput.toString().trim().length() < 8) {
                    Toast.makeText(getActivity(), "Password (at least 8 character)",
                            Toast.LENGTH_LONG).show();
                    return;
                }
            }

            if (mWifiHotspotEnable) {
                vmkoomStopWiFiHotspot();
//                MagicActivity.useMagicActivityToTurnOff(getActivity());
                return;
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                if (!Objects.requireNonNull(getActivity()).isFinishing()) {
                    new SweetAlertDialog(Objects.requireNonNull(getActivity()), SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                            .setTitleText("Start Wifi Hotspot?")
                            .setCustomImage(R.drawable.ic_free_wifi_hotspot)
                            .setContentText("for the 1st time Tap <font color='black'><b>SETUP</b></font> button to change wifi hotspot configuration")
                            .setCancelText("Cancel")
                            .setConfirmText("OK!")
                            .setNeutralText("Setup")
                            .setNeutralButtonBackgroundColor(Color.BLUE)
                            .showCancelButton(true)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    vmkoomStartWifiHotspot();
                                    starthotspotdialog_oreo();
                                    sDialog.cancel();
                                }
                            })
                            .setNeutralClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    final Intent intent = new Intent(Intent.ACTION_MAIN, null);
                                    intent.addCategory(Intent.CATEGORY_LAUNCHER);
                                    final ComponentName cn = new ComponentName("com.android.settings", "com.android.settings.TetherSettings");
                                    intent.setComponent(cn);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                }
                            })
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.cancel();
                                }
                            })
                            .show();
                }
            } else {
                vmkoomStartWifiHotspot();
                starthotspotdialog();
            }

//            vmkoomStartWifiHotspot();
//            starthotspotdialog();

        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }
    }

    public void vmkoomStartWifiHotspot() {

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            if (!Settings.System.canWrite(getActivity())) {
//                Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:" + getPackageName()));
//                startActivityForResult(intent, requestCode_ACTION_MANAGE_WRITE_SETTINGS);
//                return;
//            }
//        }
//        final Intent intent = new Intent(Intent.ACTION_MAIN, null);
//        intent.addCategory(Intent.CATEGORY_LAUNCHER);
//        final ComponentName cn = new ComponentName("com.android.settings", "com.android.settings.TetherSettings");
//        intent.setComponent(cn);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(intent);
        if (mHotUtil != null) {

            String password;
            if (vmkoomSecuritySwitchToggle.isChecked()) {
                password = vmkoomPasswordWifiHotspot;
                vmkoomSettingInformation.setText(
                        "Network name: " + vmkoomNameWifiHotspot + "\n" + "Password: " + vmkoomPasswordWifiHotspot);
            } else {
                password = "";
                vmkoomSettingInformation.setText(
                        "Network name: " + vmkoomNameWifiHotspot + "\n" + "Password: ");
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                MagicActivity.useMagicActivityToTurnOn(getActivity());
//                MagicActivity.useMagicActivityToTurnOn(Objects.requireNonNull(getActivity()));
                vmkoomSettingInformation.setVisibility(View.GONE);
//                oreo_instruction.setVisibility(View.GONE);
                btinstruction_oreo.setVisibility(View.GONE);
                hotspoton_alert.setVisibility(View.VISIBLE);
            } else {
                vmkoomSettingInformation.setVisibility(View.VISIBLE);
//                oreo_instruction.setVisibility(View.GONE);
                btinstruction_oreo.setVisibility(View.GONE);
            }
//            vmkoomSettingInformation.setVisibility(View.VISIBLE);

            //Start Hotspot
            mHotUtil.setAndStartHotSpotCheckAndroidVersion_v2(true, vmkoomNameWifiHotspot, password);
            MagicActivity.useMagicActivityToTurnOn(getApplicationContext());
            mWifiHotspotEnable = true;

//            Change Image
            vmkoomImageToggle.setImageResource(R.drawable.vmkoom_wifi_enable);
//            bthotspot_off.setVisibility(View.GONE);


            hotspotconfig.setVisibility(View.GONE);
        }


    }

    private void vmkoomStopWiFiHotspot() {
        if (mHotUtil != null) {

            vmkoomSettingInformation.setVisibility(View.GONE);

            //Stop hotspot
            mHotUtil.setAndStartHotSpotCheckAndroidVersion_v2(false, vmkoomNameWifiHotspot, vmkoomPasswordWifiHotspot);

            //Change Image
            vmkoomImageToggle.setImageResource(R.drawable.vmkoom_wifi_disable);
//            bthotspot_off.setVisibility(View.GONE);


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                hotspotconfig.setVisibility(View.GONE);
//                oreo_instruction.setVisibility(View.VISIBLE);
                btinstruction_oreo.setVisibility(View.VISIBLE);
                hotspoton_alert.setVisibility(View.GONE);
            } else {
                hotspotconfig.setVisibility(View.VISIBLE);
//                oreo_instruction.setVisibility(View.GONE);
                btinstruction_oreo.setVisibility(View.GONE);
            }
//            hotspotconfig.setVisibility(View.VISIBLE);
            mWifiHotspotEnable = false;


            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    try {
                        stophotspotdialog2();
                        MagicActivity.useMagicActivityToTurnOff(Objects.requireNonNull(getActivity()));


                    } catch (Exception ignored) {
                        Crashlytics.logException(ignored);
                    }

                }
            }, 200);
            WifiManager wifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            assert wifi != null;
            wifi.setWifiEnabled(true);
        }
    }

    private void starthotspotdialog() {
        new SweetAlertDialog(Objects.requireNonNull(getActivity()), SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Start Wifi Hotspot")
                .setContentText("Network name: " + vmkoomNameWifiHotspot + "\n" + "Password: " + vmkoomPasswordWifiHotspot)
                .setCustomImage(R.drawable.ic_free_wifi_hotspot)
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                    }
                })
                .show();
    }

    private void starthotspotdialog_oreo() {
        if (!Objects.requireNonNull(getActivity()).isFinishing()) {
            new SweetAlertDialog(Objects.requireNonNull(getActivity()), SweetAlertDialog.SUCCESS_TYPE)
                    .setTitleText("Start Wifi Hotspot")
                    .setCustomImage(R.drawable.ic_free_wifi_hotspot)
                    .setConfirmText("OK")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismissWithAnimation();
                            sweetAlertDialog.cancel();
                        }
                    })
                    .show();
        }
    }

    private void oreoinstruction_dialog() {
        if (!Objects.requireNonNull(getActivity()).isFinishing()) {
            DialogPlus dialog = DialogPlus.newDialog(Objects.requireNonNull(getActivity()))
                    .setGravity(Gravity.BOTTOM)
                    .setHeader(R.layout.footer)
//                .setFooter(R.layout.footer)
                    .setContentHolder(new ViewHolder(R.layout.diaglog_instruction))
                    .setCancelable(true)
                    .setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(DialogPlus dialog, View view) {
                            if (view.getId() == R.id.footer_close_button) {
//                            Toast.makeText(getActivity(), "Cancel click",
//                                    Toast.LENGTH_LONG).show();
                                dialog.dismiss();
                            }
                        }
                    })
                    .setOnCancelListener(new OnCancelListener() {
                        @Override
                        public void onCancel(DialogPlus dialog) {
//                        Toast.makeText(getActivity(), "Cancel dismiss",
//                                Toast.LENGTH_LONG).show();
                        }
                    })

                    .setContentHeight(ViewGroup.LayoutParams.MATCH_PARENT)
                    .setContentWidth(ViewGroup.LayoutParams.MATCH_PARENT)
                    .setMargin(0, 500, 0, 0)
                    .create();
            dialog.show();
        }
//        cancelbutton = (Button) view.findViewById(R.id.footer_close_button);
//
//        cancelbutton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View mRootView) {
//                Toast.makeText(getActivity(), "Cancel click",
//                        Toast.LENGTH_LONG).show();
//            }
//        });
    }


    private void stophotspotdialog2() {
        final SweetAlertDialog pDialog = new SweetAlertDialog(Objects.requireNonNull(getActivity()), SweetAlertDialog.PROGRESS_TYPE)
                .setTitleText("Loading");
        if (!Objects.requireNonNull(getActivity()).isFinishing()) {
            pDialog.show();
        }
        pDialog.setCancelable(false);
        new CountDownTimer(800 * 2, 800) {
            public void onTick(long millisUntilFinished) {
                // you can change the progress bar color by ProgressHelper every 800 millis
                i++;
                switch (i) {
                    case 0:
                        pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.blue_btn_bg_color));
                        break;
                    case 1:
                        pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.material_deep_teal_50));
                        break;
                    case 2:
                        pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.success_stroke_color));
                        break;
                    case 3:
                        pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.material_deep_teal_20));
                        break;
                    case 4:
                        pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.material_blue_grey_80));
                        break;
                    case 5:
                        pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.warning_stroke_color));
                        break;
                    case 6:
                        pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.success_stroke_color));
                        break;
                }
            }

            public void onFinish() {
                i = -1;

                pDialog.setTitleText("Disable Hotspot")
                        .setContentText("Personal Mobile Hotspot already Turn Off")
                        .setConfirmText("OK")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                                showInterstitial();
                            }
                        })
                        .setCustomImage(R.drawable.ic_free_wifi_hotspot)
                        .changeAlertType(SweetAlertDialog.CUSTOM_IMAGE_TYPE);

            }
        }.start();
    }

    @Override
    public boolean canScrollVertically(int direction) {
        return mScrollView != null && mScrollView.canScrollVertically(direction);
    }

    @Override
    public void onFlingOver(int y, long duration) {
        if (mScrollView != null) {
            mScrollView.smoothScrollBy(0, y);
        }
    }

//    /**
//     * Populates a {@link UnifiedNativeAdView} object with data from a given
//     * {@link UnifiedNativeAd}.
//     *
//     * @param nativeAd the object containing the ad's assets
//     * @param adView   the view to be populated
//     */
//    private void populateUnifiedNativeAdView(UnifiedNativeAd nativeAd, UnifiedNativeAdView adView) {
//        try {
//            // Get the video controller for the ad. One will always be provided, even if the ad doesn't
//            // have a video asset.
//            VideoController vc = nativeAd.getVideoController();
//
//            // Create a new VideoLifecycleCallbacks object and pass it to the VideoController. The
//            // VideoController will call methods on this object when events occur in the video
//            // lifecycle.
//            vc.setVideoLifecycleCallbacks(new VideoController.VideoLifecycleCallbacks() {
//                public void onVideoEnd() {
//                    // Publishers should allow native ads to complete video playback before refreshing
//                    // or replacing them with another ad in the same UI location.
////                refresh.setEnabled(true);
////                videoStatus.setText("Video status: Video playback has ended.");
//                    super.onVideoEnd();
//                }
//            });
//
//            MediaView mediaView = adView.findViewById(R.id.ad_media);
//            ImageView mainImageView = adView.findViewById(R.id.ad_image);
//
//            // Apps can check the VideoController's hasVideoContent property to determine if the
//            // NativeAppInstallAd has a video asset.
//            if (vc.hasVideoContent()) {
//                adView.setMediaView(mediaView);
//                mainImageView.setVisibility(View.GONE);
////            videoStatus.setText(String.format(Locale.getDefault(),
////                    "Video status: Ad contains a %.2f:1 video asset.",
////                    vc.getAspectRatio()));
//            } else {
//                adView.setImageView(mainImageView);
//                mediaView.setVisibility(View.GONE);
//
//                // At least one image is guaranteed.
//                List<com.google.android.gms.ads.formats.NativeAd.Image> images = nativeAd.getImages();
//                mainImageView.setImageDrawable(images.get(0).getDrawable());
//
////            refresh.setEnabled(true);
////            videoStatus.setText("Video status: Ad does not contain a video asset.");
//            }
//
//            adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
//            adView.setBodyView(adView.findViewById(R.id.ad_body));
//            adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
//            adView.setIconView(adView.findViewById(R.id.ad_app_icon));
////        adView.setPriceView(adView.findViewById(R.id.ad_price));
//            adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
////        adView.setStoreView(adView.findViewById(R.id.ad_store));
//            adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));
//
//            // Some assets are guaranteed to be in every UnifiedNativeAd.
//            ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());
//            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
//            ((Button) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
//
//            // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
//            // check before trying to display them.
//            if (nativeAd.getIcon() == null) {
//                adView.getIconView().setVisibility(View.GONE);
//            } else {
//                ((ImageView) adView.getIconView()).setImageDrawable(
//                        nativeAd.getIcon().getDrawable());
//                adView.getIconView().setVisibility(View.VISIBLE);
//            }
//
////        if (nativeAd.getPrice() == null) {
////            adView.getPriceView().setVisibility(View.INVISIBLE);
////        } else {
////            adView.getPriceView().setVisibility(View.VISIBLE);
////            ((TextView) adView.getPriceView()).setText(nativeAd.getPrice());
////        }
//
////        if (nativeAd.getStore() == null) {
////            adView.getStoreView().setVisibility(View.INVISIBLE);
////        } else {
////            adView.getStoreView().setVisibility(View.VISIBLE);
////            ((TextView) adView.getStoreView()).setText(nativeAd.getStore());
////        }
//
//            if (nativeAd.getStarRating() == null) {
//                adView.getStarRatingView().setVisibility(View.INVISIBLE);
//            } else {
//                ((RatingBar) adView.getStarRatingView())
//                        .setRating(nativeAd.getStarRating().floatValue());
//                adView.getStarRatingView().setVisibility(View.VISIBLE);
//            }
//
//            if (nativeAd.getAdvertiser() == null) {
//                adView.getAdvertiserView().setVisibility(View.INVISIBLE);
//            } else {
//                ((TextView) adView.getAdvertiserView()).setText(nativeAd.getAdvertiser());
//                adView.getAdvertiserView().setVisibility(View.VISIBLE);
//            }
//
//            adView.setNativeAd(nativeAd);
//
//        } catch (Exception ignored) {
//            Crashlytics.logException(ignored);
//        }
//    }

    /**
     * Creates a request for a new native ad based on the boolean parameters and calls the
     * corresponding "populate" method when one is successfully returned.
     */
    private void refreshAd() {
        try {
//        refresh.setEnabled(false);

            AdLoader adLoader = new AdLoader.Builder(requireActivity(), getResources().getString(R.string.admob_large_native_id))
                    .forNativeAd(new NativeAd.OnNativeAdLoadedListener() {
                        @Override
                        public void onNativeAdLoaded(NativeAd nativeAd) {
                            // Show the ad.
                            NativeTemplateStyle styles = new
                                    NativeTemplateStyle.Builder().build();
                            TemplateView template = view.findViewById(R.id.my_template);
//                        template.setStyles(styles);
//                        template.setNativeAd(nativeAd);
                            if (nativeAd == null) {
                                template.setVisibility(View.GONE);
                            } else {
//                                template.setVisibility(View.VISIBLE);
                                template.setNativeAd(nativeAd);
                            }
                        }
                    })
                    .withAdListener(new AdListener() {
                        @Override
                        public void onAdFailedToLoad(LoadAdError adError) {
                            // Handle the failure by logging, altering the UI, and so on.
                        }
                    })
                    .withNativeAdOptions(new NativeAdOptions.Builder()
                            // Methods in the NativeAdOptions.Builder class can be
                            // used here to specify individual options settings.
                            .build())
                    .build();
            adLoader.loadAd(new AdRequest.Builder().build());

//            AdLoader.Builder builder2 = new AdLoader.Builder(Objects.requireNonNull(getActivity()), getResources().getString(R.string.admob_large_native_id));
//
//            builder2.forUnifiedNativeAd(new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
//                // OnUnifiedNativeAdLoadedListener implementation.
////                @Override
////                public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd2) {
////                    try {
////                        FrameLayout frameLayout2 =
////                                mView.findViewById(R.id.fl_adplaceholder2);
////                        UnifiedNativeAdView adView2 = (UnifiedNativeAdView) getLayoutInflater()
////                                .inflate(R.layout.ad_unified, null);
////                        populateUnifiedNativeAdView(unifiedNativeAd2, adView2);
////                        frameLayout2.removeAllViews();
////                        frameLayout2.addView(adView2);
////                    } catch (Exception e) {
////                        FirebaseCrashlytics.getInstance().recordException(e);
////                    }
////                }
//                @Override
//                public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
//                    NativeTemplateStyle styles = new
//                            NativeTemplateStyle.Builder().build();
//
//                    TemplateView template = mRootView.findViewById(R.id.my_template);
//                    template.setStyles(styles);
//                    template.setNativeAd(unifiedNativeAd);
//                    template.setVisibility(View.VISIBLE);
//                }
//
//            });
//
//
//            VideoOptions videoOptions = new VideoOptions.Builder()
//                    .build();
//
//            NativeAdOptions adOptions = new NativeAdOptions.Builder()
//                    .setVideoOptions(videoOptions)
//                    .build();
//
//            builder2.withNativeAdOptions(adOptions);
//
//            AdLoader adLoader2 = builder2.withAdListener(new AdListener() {
//                @Override
//                public void onAdFailedToLoad(LoadAdError adError) {
////                refresh.setEnabled(true);
////                Toast.makeText(MainActivity.this, "Failed to load native ad: "
////                        + errorCode, Toast.LENGTH_SHORT).show();
//                }
//            }).build();
//
//            adLoader2.loadAd(new AdRequest.Builder().build());

//        videoStatus.setText("");

        } catch (Exception e) {
            FirebaseCrashlytics.getInstance().recordException(e);
        }
    }


    private void loadBanner() {
        // Create an ad request. Check your logcat output for the hashed device ID
        // to get test ads on a physical device, e.g.,
        // "Use AdRequest.Builder.addTestDevice("ABCDE0123") to get test ads on this
        // device."
        adtView = new AdView(getActivity());
        adtView.setAdUnitId(getResources().getString(R.string.admob_adaptive_banner_id));
        adContainerView.removeAllViews();
        adContainerView.addView(adtView);

        AdRequest adRequest = new AdRequest.Builder().build();

        AdSize adSize = getAdSize();
        // Step 4 - Set the adaptive ad size on the ad view.
        adtView.setAdSize(adSize);


        // Step 5 - Start loading the ad in the background.
        adtView.loadAd(adRequest);
    }

    private com.google.android.gms.ads.AdSize getAdSize() {
        // Step 2 - Determine the screen width (less decorations) to use for the ad width.
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);

        // Step 3 - Get adaptive ad size and return for setting on the ad view.
        return com.google.android.gms.ads.AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(getActivity(), adWidth);
    }

}
