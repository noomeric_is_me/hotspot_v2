package vmkoomNTLfreewifi;

import android.app.Activity;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;


import com.airbnb.lottie.LottieAnimationView;
import com.free.ap.freehotspot.personal.mobilehotspotfree.BuildConfig;
import com.free.ap.freehotspot.personal.mobilehotspotfree.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
//import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;

import vmkoomNTLfreewifi.main.CustomSlide;
import vmkoomNTLfreewifi.main.IntroActivity;
import vmkoomNTLfreewifi.main.MainAppActivity_v2;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;


import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import static android.content.ContentValues.TAG;


public class Splashscreen extends AppCompatActivity {
    CoordinatorLayout coordinatorLayout;
    private static final int LONG_DURATION_MS = 2750;
    LottieAnimationView lottieAnimationView;

    private InterstitialAd mInterstitialAd;
    // Remote Config keys

    private static final String CONFIG_KEY_NATIVE_LARGE_DISPLAY = "display_native_large";
    private static final String CONFIG_KEY_NATIVE_SMALL_DISPLAY = "display_native_small";
    private static final String CONFIG_KEY_FULL_OPEN_APP_DISPLAY = "display_full_open_app";

    private FirebaseRemoteConfig mFirebaseRemoteConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomSlide.setMSettingPermission(false);
        setContentView(R.layout.splashscreen);
        coordinatorLayout = findViewById(R.id.cordi);
        lottieAnimationView = findViewById(R.id.animation_view);
        // Create Remote Config Setting to enable developer mode.
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();

        // Create Remote Config Setting to enable developer mode.
        // Fetching configs from the server is normally limited to 5 requests per hour.
        // Enabling developer mode allows many more requests to be made per hour, so developers
        // can test different config values during development.
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings
                .Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        mFirebaseRemoteConfig.setConfigSettingsAsync(configSettings);

        // Set default Remote Config values. In general you should have in app defaults for all
        // values that you may configure using Remote Config later on. The idea is that you
        // use the in app defaults and when you need to adjust those defaults, you set an updated
        // value in the App Manager console. Then the next time you application fetches from the
        // server, the updated value will be used. You can set defaults via an xml file like done
        // here or you can set defaults inline by using one of the other setDefaults methods.
        mFirebaseRemoteConfig.setDefaultsAsync(R.xml.remote_config_defaults);
        initremoteconfig();
        if (!Utility2.isOnline(getApplicationContext())) {


            Snackbar snackbar2 = Snackbar
                    .make(coordinatorLayout, "Check internet connection", Snackbar.LENGTH_LONG);
            snackbar2.show();


        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    Intent i = new Intent(Splashscreen.this, MainAppActivity_v2.class);
                    startActivity(i);
                    finish();
                    showInterstitial();
                }
            }, 3000);
//            startActivity(new Intent(this, IntroActivity.class));
//            finish();
        }

//        //Toast.makeText(this, "initialize in Splash", //Toast.LENGTH_SHORT).show();
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, "Loading , Please wait...", Snackbar.LENGTH_INDEFINITE);
        snackbar.show();

        AdRequest adRequest = new AdRequest.Builder().build();

        InterstitialAd.load(this, getResources().getString(R.string.admob_interstitial_id), adRequest,
                new InterstitialAdLoadCallback() {
                    @Override
                    public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                        // The mInterstitialAd reference will be null until
                        // an ad is loaded.
                        mInterstitialAd = interstitialAd;

                        Log.i(TAG, "onAdLoaded");
                    }

                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        // Handle the error
                        Log.i(TAG, loadAdError.getMessage());
                        mInterstitialAd = null;
                    }
                });
        // Added in Application class //// Added in Application class //Base.initialize(this);
    }


    private void initremoteconfig() {
//        mPriceTextView.setText(R.string.loading);

        long cacheExpiration = 3600; // 1 hour in seconds.

        // If in developer mode cacheExpiration is set to 0 so each fetch will retrieve values from the server.
        if (mFirebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }

        // cacheExpirationSeconds is set to cacheExpiration here, indicating that any previously
        // fetched and cached config would be considered expired because it would have been fetched
        // more than cacheExpiration seconds ago. Thus the next fetch would go to the server unless
        // throttling is in progress. The default expiration duration is 43200 (12 hours).
        mFirebaseRemoteConfig.fetch(cacheExpiration).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Log.d("FETCH", "Succeeded");
                    // Once the config is successfully fetched it must be activated before newly fetched values are returned.
                    mFirebaseRemoteConfig.activateFetched();
                } else {
                    Log.d("FETCH", "Failed");
                }
//                displayPrice();
            }
        });
    }

    public void showInterstitial() {
//        if (mFirebaseRemoteConfig.getBoolean(CONFIG_KEY_FULL_OPEN_APP_DISPLAY)) {
            //show native
            if (mInterstitialAd != null&&mFirebaseRemoteConfig.getBoolean(CONFIG_KEY_FULL_OPEN_APP_DISPLAY)) {
                mInterstitialAd.show(this);
            } else {
                Log.d("TAG", "The interstitial wasn't loaded yet.");
            }
//                Toast.makeText(getActivity(), "CONFIG_KEY_NATIVE_LARGE_DISPLAY - True", Toast.LENGTH_SHORT).show();

//        }
    }
}
