package vmkoomNTLspeedtest.fragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.core.widget.NestedScrollView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.google.android.gms.ads.AdView;
import com.free.ap.freehotspot.personal.mobilehotspotfree.R;

import vmkoomNTLfreewifi.activity.ChartActivity;
import vmkoomNTLfreewifi.activity.SpeedTestMiniActivity;
import vmkoomNTLfreewifi.activity.WifiConnectActivity;
import vmkoomNTLfreewifi.application.AppController;
import vmkoomNTLfreewifi.main.MainAppActivity;
import vmkoomNTLfreewifi.main.MapsMarkerActivity;
import vmkoomNTLmainapp.scrollable.fragment.FragmentPagerFragment;
import utils.AppUtils;
import vmkoomNTLwifimap.activity.MapActivity;

/**
 * Created by NTL on 8/12/2017 AD.
 */

public class MainFreeWiFiFragment extends FragmentPagerFragment {

    public static final String TAG = MainFreeWiFiFragment.class.getSimpleName();

    private View mView;
    private TextView text_number_wifi_in_range;
    private NestedScrollView mScrollView;
    private MainListener mMainListener;

    public interface MainListener{
        void clickHotspot();
    }

    public void setMainListener(MainListener mainListener){
        mMainListener = mainListener;
    }

    public static MainFreeWiFiFragment newInstance(){
        return new MainFreeWiFiFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if(mView == null){
            mView = inflater.inflate(R.layout.main_vmkoomfreewifi_fragment, container, false);

//            mScrollView = mView.findViewById(R.id.scrollView);

            mScrollView = (NestedScrollView) mView.findViewById(R.id.scrollView);
//            MaterialViewPagerHelper.registerScrollView(getActivity(), mScrollView);

            text_number_wifi_in_range = (TextView) mView.findViewById(R.id.text_number_wifi_in_range);

            TextView txt_wifi_connect = (TextView) mView.findViewById(R.id.txt_wifi_connect);
            txt_wifi_connect.setOnClickListener(mClick);

            TextView txt_wifi_speed_boost = (TextView) mView.findViewById(R.id.txt_wifi_speed_boost);
            txt_wifi_speed_boost.setOnClickListener(mClick);

            TextView txt_wifi_chart = (TextView) mView.findViewById(R.id.txt_wifi_analyzer);
            txt_wifi_chart.setOnClickListener(mClick);

            TextView txt_wifi_map = (TextView) mView.findViewById(R.id.txt_wifi_map);
            txt_wifi_map.setOnClickListener(mClick);

            TextView txt_wifi_speed_test = (TextView) mView.findViewById(R.id.txt_wifi_speed_test);
            txt_wifi_speed_test.setOnClickListener(mClick);

            TextView txt_wifi_hotspot = (TextView) mView.findViewById(R.id.txt_wifi_hotspot);
            txt_wifi_hotspot.setOnClickListener(mClick);

            TextView txt_wifi_map_explorer = (TextView) mView.findViewById(R.id.txt_wifi_map_explorer);
            txt_wifi_map_explorer.setOnClickListener(mClick);

            initAds();
        }

        return mView;
    }

    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onDestroy() {
//        if(adViewfacebook != null){
//            adViewfacebook.destroy();
//        }
        super.onDestroy();
    }

//    private com.facebook.ads.AdView adViewfacebook;
    private void initAds(){

        if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
            //Banner
            AdView mAdView = (AdView) mView.findViewById(R.id.admob_banner_view);
            mAdView.setVisibility(View.VISIBLE);
            AppUtils.getInstance().showAdsBanner(mAdView);

            //load large ads native bottom
//            NativeExpressAdView adViewNative = (NativeExpressAdView) mView.findViewById(R.id.adViewNative);
//            adViewNative.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showNativeAdsBanner(adViewNative);

//            NativeExpressAdView adViewNative_1 = (NativeExpressAdView) mView.findViewById(R.id.adViewNative_1);
//            adViewNative_1.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showNativeAdsBanner(adViewNative_1);
        } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
            //Banner
            RelativeLayout adViewContainer = (RelativeLayout) mView.findViewById(R.id.facebook_banner_ad_container);
            adViewContainer.setVisibility(View.VISIBLE);
//            adViewfacebook = AppUtils.getInstance().showFBAdsBanner(getActivity(),adViewContainer);

            //Native
            LinearLayout adNativeViewContainer = (LinearLayout) mView.findViewById(R.id.native_ad_container);
            adNativeViewContainer.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showFBAdsNativeSmallInit(getActivity(),adNativeViewContainer);

            LinearLayout adNativeViewContainer_1 = (LinearLayout) mView.findViewById(R.id.native_ad_container_1);
            adNativeViewContainer_1.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showFBAdsNativeInit(getActivity(),adNativeViewContainer_1);
        }
    }


    public void setWiFiNumberInRange(String numbers){
        if(text_number_wifi_in_range != null){
            text_number_wifi_in_range.setText(numbers);
        }
    }

    public void setWiFiHotspotMenuText(String menuText){
        if(mView != null){
            TextView txt_wifi_hotspot = (TextView) mView.findViewById(R.id.txt_wifi_hotspot);
            if(txt_wifi_hotspot != null){
                txt_wifi_hotspot.setText(menuText);
            }
        }
    }

    private View.OnClickListener mClick = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            final Intent intent;
            int id = view.getId();
            switch (id) {
                case R.id.txt_wifi_connect:
                    try {

                        Answers.getInstance().logContentView(new ContentViewEvent()
                                .putContentName("Free-WiFi-Tab - Fine More Networks"));

//                        if (AppUtils.ads_interstitial_show_all) {
//
//                            if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                                AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(getActivity(), WifiConnectActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//                                intent = new Intent(getActivity(), WifiConnectActivity.class);
//                                startActivity(intent);
//                            }
//
//                        } else {
//
//                            if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                                AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(getActivity(), WifiConnectActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                                AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(getActivity(), WifiConnectActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//                                intent = new Intent(getActivity(), WifiConnectActivity.class);
//                                startActivity(intent);
//                            }
//                        }

                        intent = new Intent(getActivity(), WifiConnectActivity.class);
                        startActivity(intent);

                    } catch (Exception ignored) {
                        Crashlytics.logException(ignored);
                    }
                    break;
//                case R.id.txt_wifi_speed_boost:
//                    try {
//
//                        Answers.getInstance().logContentView(new ContentViewEvent()
//                                .putContentName("Free-WiFi-Tab - Wifi Speed Booster"));
//
////                        if (AppUtils.ads_interstitial_show_all) {
////
////                            if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
////                                AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
////                                    @Override
////                                    public void onAdClosed() {
////                                        Intent intent = new Intent(getActivity(), OptimizeActivity.class);
////                                        intent.putExtra(MainAppActivity.COLOR_MESSAGE, MainAppActivity.currentColor);
////                                        startActivity(intent);
////                                    }
////                                });
////                            } else {
////                                intent = new Intent(getActivity(), OptimizeActivity.class);
////                                intent.putExtra(MainAppActivity.COLOR_MESSAGE, MainAppActivity.currentColor);
////                                startActivity(intent);
////                            }
////
////                        } else {
////
////                            if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
////                                AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
////                                    @Override
////                                    public void onAdClosed() {
////                                        Intent intent = new Intent(getActivity(), OptimizeActivity.class);
////                                        intent.putExtra(MainAppActivity.COLOR_MESSAGE, MainAppActivity.currentColor);
////                                        startActivity(intent);
////                                    }
////                                });
////                            } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
////                                AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
////                                    @Override
////                                    public void onAdClosed() {
////                                        Intent intent = new Intent(getActivity(), OptimizeActivity.class);
////                                        intent.putExtra(MainAppActivity.COLOR_MESSAGE, MainAppActivity.currentColor);
////                                        startActivity(intent);
////                                    }
////                                });
////                            } else {
////                                intent = new Intent(getActivity(), OptimizeActivity.class);
////                                intent.putExtra(MainAppActivity.COLOR_MESSAGE, MainAppActivity.currentColor);
////                                startActivity(intent);
////                            }
////                        }
//
//                        intent = new Intent(getActivity(), OptimizeActivity.class);
//                        intent.putExtra(MainAppActivity.COLOR_MESSAGE, MainAppActivity.currentColor);
//                        startActivity(intent);
//
//                    } catch (Exception ignored) {
//                        Crashlytics.logException(ignored);
//                    }
//                    break;
                case R.id.txt_wifi_analyzer:
                    try {

                        Answers.getInstance().logContentView(new ContentViewEvent()
                                .putContentName("Free-WiFi-Tab - Wifi Analyzer"));

//                        if (AppUtils.ads_interstitial_show_all) {
//
//                            if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                                AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(getActivity(), ChartActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//                                intent = new Intent(getActivity(), ChartActivity.class);
//                                startActivity(intent);
//                            }
//
//                        } else {
//
//                            if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                                AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(getActivity(), ChartActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                                AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(getActivity(), ChartActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//                                intent = new Intent(getActivity(), ChartActivity.class);
//                                startActivity(intent);
//                            }
//                        }

                        intent = new Intent(getActivity(), ChartActivity.class);
                        startActivity(intent);

                    } catch (Exception ignored) {
                        Crashlytics.logException(ignored);
                    }

                    break;
                case R.id.txt_wifi_map:
                    try {

                        Answers.getInstance().logContentView(new ContentViewEvent()
                                .putContentName("Free-WiFi-Tab - Network Map Location"));

                        final double lat;
                        final double lng;

                        if(AppUtils.getInstance().getAppNetworkInfo() == null || AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation() == null) {
                            lat = 0;
                            lng = 0;
                        }else {
                            lat = Double.parseDouble(AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation().getLat());
                            lng = Double.parseDouble(AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation().getLon());
                        }

//                        if (AppUtils.ads_interstitial_show_all) {
//
//                            if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                                AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(AppController.getInstance().getAppContext(), MapsMarkerActivity.class);
//                                        intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, lat);
//                                        intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, lng);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//                                intent = new Intent(AppController.getInstance().getAppContext(), MapsMarkerActivity.class);
//                                intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, lat);
//                                intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, lng);
//                                startActivity(intent);
//                            }
//
//                        } else {
//
//                            if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                                AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(AppController.getInstance().getAppContext(), MapsMarkerActivity.class);
//                                        intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, lat);
//                                        intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, lng);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                                AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(AppController.getInstance().getAppContext(), MapsMarkerActivity.class);
//                                        intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, lat);
//                                        intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, lng);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//
//                                intent = new Intent(AppController.getInstance().getAppContext(), MapsMarkerActivity.class);
//                                intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, lat);
//                                intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, lng);
//                                startActivity(intent);
//                            }
//                        }

                        intent = new Intent(AppController.getInstance().getAppContext(), MapsMarkerActivity.class);
                        intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, lat);
                        intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, lng);
                        startActivity(intent);

                    } catch (Exception ignored) {
                        Crashlytics.logException(ignored);
                    }
                    break;
                case R.id.txt_wifi_map_explorer:
                    try {

                        Answers.getInstance().logContentView(new ContentViewEvent()
                                .putContentName("Free-WiFi-Tab - Wi-Fi Map Explorer"));

//                        if (AppUtils.ads_interstitial_show_all) {
//
//                            if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                                AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//                                intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
//                                startActivity(intent);
//                            }
//
//                        } else {
//
//                            if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                                AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                                AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//                                intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
//                                startActivity(intent);
//                            }
//                        }

                        intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
                        startActivity(intent);

                    } catch (Exception ignored) {
                        Crashlytics.logException(ignored);
                    }

                    break;
                case R.id.txt_wifi_speed_test:
                    try {

                        Answers.getInstance().logContentView(new ContentViewEvent()
                                .putContentName("Free-WiFi-Tab - Wifi Speed Test"));

//                        if (AppUtils.ads_interstitial_show_all) {
//
//                            if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                                AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(getActivity(), SpeedTestMiniActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//                                intent = new Intent(getActivity(), SpeedTestMiniActivity.class);
//                                startActivity(intent);
//                            }
//
//                        } else {
//
//                            if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                                AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(getActivity(), SpeedTestMiniActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                                AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        Intent intent = new Intent(getActivity(), SpeedTestMiniActivity.class);
//                                        startActivity(intent);
//                                    }
//                                });
//                            } else {
//                                intent = new Intent(getActivity(), SpeedTestMiniActivity.class);
//                                startActivity(intent);
//                            }
//                        }

                        intent = new Intent(getActivity(), SpeedTestMiniActivity.class);
                        startActivity(intent);

                    } catch (Exception ignored) {
                        Crashlytics.logException(ignored);
                    }
                    break;

                case R.id.txt_wifi_hotspot:

                    try {

                        Answers.getInstance().logContentView(new ContentViewEvent()
                                .putContentName("Free-WiFi-Tab - Free WiFi Hotspot"));

//                        if (AppUtils.ads_interstitial_show_all) {
//
//                            if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                                AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        if(mMainListener != null){
//                                            mMainListener.clickHotspot();
//                                        }
//
//                                    }
//                                });
//                            } else {
//                                if(mMainListener != null){
//                                    mMainListener.clickHotspot();
//                                }
//
//                            }
//
//                        } else {
//
//                            if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                                AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        if(mMainListener != null){
//                                            mMainListener.clickHotspot();
//                                        }
//
//                                    }
//                                });
//                            } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                                AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                                    @Override
//                                    public void onAdClosed() {
//                                        if(mMainListener != null){
//                                            mMainListener.clickHotspot();
//                                        }
//
//                                    }
//                                });
//                            } else {
//                                if(mMainListener != null){
//                                    mMainListener.clickHotspot();
//                                }
//
//                            }
//                        }

                        if(mMainListener != null){
                            mMainListener.clickHotspot();
                        }

                    } catch (Exception ignored) {
                        Crashlytics.logException(ignored);
                    }
                    break;

            }
        }
    };

    @Override
    public boolean canScrollVertically(int direction) {
        return mScrollView != null && mScrollView.canScrollVertically(direction);
    }

    @Override
    public void onFlingOver(int y, long duration) {
        if (mScrollView != null) {
            mScrollView.smoothScrollBy(0, y);
        }
    }
}
