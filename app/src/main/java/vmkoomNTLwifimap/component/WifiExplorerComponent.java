package vmkoomNTLwifimap.component;

import javax.inject.Singleton;

import dagger.Component;
import vmkoomNTLfreewifi.application.AppController;
import vmkoomNTLfreewifi.main.MainAppActivity_v2;
import vmkoomNTLwifimap.activity.MapActivity;
import vmkoomNTLwifimap.module.DaggerModule;

/**
 * Created by Federico
 */
@Singleton
@Component(modules = DaggerModule.class)
public interface WifiExplorerComponent {
    void inject(AppController application);
    void inject(MainAppActivity_v2 mainActivity);
    void inject(MapActivity mapActivity);
}
