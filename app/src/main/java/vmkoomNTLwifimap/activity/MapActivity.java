package vmkoomNTLwifimap.activity;

import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.crashlytics.android.Crashlytics;
import com.free.ap.freehotspot.personal.mobilehotspotfree.R;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import vmkoomNTLfreewifi.application.AppController;
import utils.AppUtils;
import vmkoomNTLwifimap.activity.dialog.WifiLocationDialog;
import vmkoomNTLwifimap.activity.dialog.WifiSecureDialog;
import vmkoomNTLwifimap.model.db.DataBaseHandler;
import vmkoomNTLwifimap.model.location.LocationHandler;
import vmkoomNTLwifimap.model.location.LocationKeeper;
import vmkoomNTLwifimap.model.wifi.WifiElement;
import vmkoomNTLwifimap.model.wifi.WifiKeeper;
import vmkoomNTLwifimap.model.wifi.container.strategy.sortedlist.WifiList;
import vmkoomNTLwifimap.ui.presenter.FilterDelegate;

/**
 * Created by Federico
 */
public class MapActivity extends AppCompatActivity implements
        OnMapReadyCallback, WifiLocationDialog.WifiLocationDialogListener,
        WifiSecureDialog.WifiSecureDialogListener {

    public static final String EXTRA_WIFI_FAVOURITES = "wifi_list";
    private static final int ZOOM_LEVEL = 18;

    private GoogleMap map;
    private boolean favouritesMap;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    WifiKeeper wifiKeeper;

    @Inject
    LocationHandler locationHandler;

    @Inject
    DataBaseHandler dataBaseHandler;

    @Inject
    FilterDelegate filterDelegate;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi_map);

        initializeInjectors();
        initializeViewComponents();

        //Init admob or facebook
        initAds();
    }

    private void initializeInjectors() {
        ButterKnife.bind(this);
        ((AppController) getApplication()).getComponent().inject(this);
    }

    private void initializeViewComponents() {
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setTitle(R.string.activity_map_title);
        }

        favouritesMap = getIntent().getBooleanExtra(EXTRA_WIFI_FAVOURITES, false);
        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap map) {
        this.map = map;

        if (favouritesMap)
            renderMap(map, dataBaseHandler.getList());
        else
            renderMap(map, wifiKeeper.getFilteredList());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_map_actions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                finish();
                onBackPressed();
                return true;

            case R.id.map_filter_location:
                DialogFragment wifiLocationDialog = new WifiLocationDialog();
                wifiLocationDialog.show(getSupportFragmentManager(), "WifiLocationDialog");
                return true;

            case R.id.map_filter_secure:
                DialogFragment wifiSecureDialog = new WifiSecureDialog();
                wifiSecureDialog.show(getSupportFragmentManager(), "WifiLocationDialog");
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDialogNearbyClick(DialogFragment dialog) {
        filterDelegate.showNearbyWifiList();
        renderMap(map, wifiKeeper.getFilteredList());
    }

    @Override
    public void onDialogSessionClick(DialogFragment dialog) {
        filterDelegate.showSessionWifiList();
        renderMap(map, wifiKeeper.getFilteredList());
    }

    @Override
    public void onDialogFavouriteClick(DialogFragment dialog) {
        renderMap(map, dataBaseHandler.getList());
    }

    @Override
    public void onDialogOpenNetworksClick(DialogFragment dialog) {
        filterDelegate.showOnlyOpenNetworks();
        renderMap(map, wifiKeeper.getFilteredList());
    }

    @Override
    public void onDialogSecureNetworksClick(DialogFragment dialog) {
        filterDelegate.showOnlyClosedNetworks();
        renderMap(map, wifiKeeper.getFilteredList());
    }

    @Override
    public void onDialogAllNetworksClick(DialogFragment dialog) {
        filterDelegate.showAllNetworks();
        renderMap(map, wifiKeeper.getFilteredList());
    }

    private void renderMap(GoogleMap map, WifiList wifiList) {

        if(map == null) return;

        map.clear();
        map.setMyLocationEnabled(true);

        final LatLng currentLatLng = locationHandler.getCurrentLatLng();
        if (currentLatLng != null) {
            final CameraUpdate locationCamera = CameraUpdateFactory.newLatLngZoom(currentLatLng, ZOOM_LEVEL);
            map.animateCamera(locationCamera);
        }

        for(WifiElement wifiElement : wifiList) {
            LocationKeeper locationKeeper = locationHandler.get(wifiElement.getBSSID());

            if (locationKeeper == null)
                continue;

            LatLng center = locationKeeper.getCenter().getLocation();

            map.addMarker(new MarkerOptions().position(center).title(wifiElement.getSSID()));

            CircleOptions options = new CircleOptions();
            options.center(center);
            options.radius(locationKeeper.getCenter().getRadius());
            options.fillColor(wifiElement.getLightColor());
            options.strokeColor(wifiElement.getBoldColor());
            options.strokeWidth(5);

            map.addCircle(options);
        }
    }

//    private com.facebook.ads.AdView adViewfacebook;
    private void initAds(){

        if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
            //Show Admob Ads

            //Banner
            AdView mAdView = (AdView) findViewById(R.id.admob_banner_view);
            mAdView.setVisibility(View.VISIBLE);
            AppUtils.getInstance().showAdsBanner(mAdView);

        } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
            //Show Facebook Ads

            RelativeLayout adViewContainer = (RelativeLayout) findViewById(R.id.adViewContainer);
//            adViewfacebook = AppUtils.getInstance().showFBAdsBanner(this,adViewContainer);

        }
    }

    @Override
    public void onDestroy() {
//        if(adViewfacebook != null){
//            adViewfacebook.destroy();
//        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

        try {
            if(AppUtils.ads_interstitial_show_all) {

                if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
                    AppUtils.getInstance().showAdsFullBanner(null);
                }

            }else {

                if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {

                    AppUtils.getInstance().showAdmobAdsFullBanner(null);

                } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {

//                    AppUtils.getInstance().showFBAdsFullBanner(null);

                }
            }
        } catch (Exception ignored) {
            Crashlytics.logException(ignored);
        }

        super.onBackPressed();
    }

}