package vmkoomNTLwifimap.model.wifi.container;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import vmkoomNTLwifimap.model.wifi.WifiElement;
import vmkoomNTLwifimap.model.wifi.container.strategy.CurrentWifiList;
import vmkoomNTLwifimap.model.wifi.container.strategy.SessionWifiList;
import vmkoomNTLwifimap.model.wifi.container.strategy.WifiListPopulate;
import vmkoomNTLwifimap.model.wifi.container.strategy.sortedlist.WifiList;

/**
 * Created by Federico
 */
public class WifiListContainer {

    private final Map<WifiListEnum, WifiListPopulate> mapList = new HashMap<>();

    public WifiListContainer(CurrentWifiList currentWifiList, SessionWifiList sessionWifiList) {
        mapList.put(WifiListEnum.NEAR, currentWifiList);
        mapList.put(WifiListEnum.SESSION, sessionWifiList);
    }

    public WifiList getList(WifiListEnum wifiListEnum) {
        return (WifiList) mapList.get(wifiListEnum);
    }

    public void populate(List<WifiElement> list) {
        for (WifiListPopulate wifiList : mapList.values()) {
            wifiList.populate(list);
        }
    }
}
