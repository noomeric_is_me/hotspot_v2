package vmkoomNTLwifimap.model.wifi.container.strategy;


import java.util.List;

import vmkoomNTLwifimap.model.wifi.WifiElement;
import vmkoomNTLwifimap.model.wifi.container.strategy.sortedlist.WifiList;

/**
 * Created by Federico
 */
public class SessionWifiList extends WifiList implements WifiListPopulate {
    @Override
    public void populate(List<WifiElement> wifiElementList) {
        for (WifiElement wifiElement : this) {
            wifiElement.invalidate();
        }

        for (WifiElement wifiElement : wifiElementList) {
            addUpdate(wifiElement, true);
        }
    }
}
