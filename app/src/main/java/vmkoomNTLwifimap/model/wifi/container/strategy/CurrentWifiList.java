package vmkoomNTLwifimap.model.wifi.container.strategy;

import java.util.List;

import vmkoomNTLwifimap.model.wifi.WifiElement;
import vmkoomNTLwifimap.model.wifi.container.strategy.sortedlist.WifiList;

/**
 * Created by Federico
 */
public class CurrentWifiList extends WifiList implements WifiListPopulate {
    @Override
    public void populate(List<WifiElement> wifiElementList) {
        clear();
        addAll(wifiElementList);
    }
}
