package utils;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;

import com.airbnb.lottie.LottieAnimationView;
import com.free.ap.freehotspot.personal.mobilehotspotfree.R;
import com.google.android.ads.nativetemplates.TemplateView;
import com.google.android.gms.ads.nativead.NativeAd;


public class ExitDialog extends Dialog {
    LottieAnimationView lottieAnimationView;
    NativeAd ad;
    Activity activity;
    public ExitDialog(Activity activity, NativeAd ad)

    {
        super(activity);
        this.activity = activity;
        this.ad = ad;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.exit_layout);
        lottieAnimationView = findViewById(R.id.animation_view);

//        Button yes =findViewById(R.id.btn_yes);
//        Button no =findViewById(R.id.btn_no);

        LinearLayout yes =findViewById(R.id.exit_exit_prompt);
        LinearLayout no =findViewById(R.id.close_exit_prompt);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.finish();
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        TemplateView ad = findViewById(R.id.ad_template);
        if(this.ad == null)
        {
            ad.setVisibility(View.GONE);
        }
        else
        {
            ad.setVisibility(View.VISIBLE);
            ad.setNativeAd(this.ad);
        }
    }
}
